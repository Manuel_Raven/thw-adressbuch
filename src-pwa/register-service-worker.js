import { register } from 'register-service-worker'

// The ready(), registered(), cached(), updatefound() and updated()
// events passes a ServiceWorkerRegistration instance in their arguments.
// ServiceWorkerRegistration: https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration

register(process.env.SERVICE_WORKER_FILE, {
  // The registrationOptions object will be passed as the second argument
  // to ServiceWorkerContainer.register()
  // https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerContainer/register#Parameter

  // registrationOptions: { scope: './' },

  ready(/* registration */) {
    console.log('PWA: Service worker is active.')
  },

  async registered(/* registration */) {
    console.log('PWA: Service worker has been registered.')
    const status = await navigator.permissions.query({
      name: 'periodic-background-sync',
    });
    console.log(status)
    if (status.state === 'granted') {
      console.log('PWA: Granted Periodic BG-Sync')
    } else {
      console.log('PWA: NOT Granted Periodic BG-Sync')
    }
  },

  cached(/* registration */) {
    console.log('PWA: Content has been cached for offline use.')
  },

  updatefound(/* registration */) {
    console.log('PWA: New content is downloading.')
  },

  updated(/* registration */) {
    console.log('PWA: New content is available; please refresh.')
    window.location.reload()
  },

  offline() {
    console.log('PWA: No internet connection found. App is running in offline mode.')
  },

  error(err) {
    console.error('PWA: Error during service worker registration:', err)
  }
})
