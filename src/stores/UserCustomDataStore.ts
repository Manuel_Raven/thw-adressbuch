import { defineStore } from 'pinia';

export interface DstCustomData {
  dst_id: number;
  contacts: {
    type: string;
    title: string;
    value: string;
  }[];
  notes: string;
}

export const useUserCustomDataStore = defineStore('UserCustomDataStore', {
  state: () => ({
    isCustomDataActive: false,
    custom_info: [
      {
        dst_id: 1,
        contacts: [
          { type: 'tel', title: 'eine Nummer', value: '0911 319 0' },
          { type: 'web', title: 'Jugendgruppe', value: 'thw-jugend.de' },
          {
            type: 'location',
            title: 'Übungsgelände',
            value: 'Auf der Lauer 33',
          },
        ],
        notes: 'Sind besonders gesund',
      },
    ],
  }),
  getters: {
    getCustomInfo: (state) => (dst_id: number) => {
      return state.custom_info.find((x) => x.dst_id === dst_id);
    },
  },
  actions: {
    AddContact(
      dst_id: number,
      contact: {
        type: string;
        title: string;
        value: string;
      }
    ) {
      const index = this.custom_info.findIndex((x) => x.dst_id === dst_id);
      if (index === -1) {
        this.custom_info.push({
          dst_id,
          contacts: [contact],
          notes: '',
        });
      } else {
        this.custom_info[index].contacts.push(contact);
      }
    },
    DeleteContactAtIndex(dst_id: number, index: number) {
      const index_of_dst = this.custom_info.findIndex(
        (x) => x.dst_id === dst_id
      );
      if (index_of_dst === -1) return;
      this.custom_info[index_of_dst].contacts.splice(index, 1);
    },
    UpdateContactNote(dst_id: number, note: string) {
      const index_of_dst = this.custom_info.findIndex(
        (x) => x.dst_id === dst_id
      );
      if (index_of_dst === -1) {
        this.custom_info.push({
          dst_id,
          contacts: [],
          notes: note,
        });
      } else {
        this.custom_info[index_of_dst].notes = note;
      }
    },
  },
  persist: true,
});
