import { defineStore } from 'pinia';
import { ref } from 'vue';
export const useVersionStore = defineStore('VersionStore', () => {
  const packageVersion = ref<string>(process.env.PACKAGE_VERSION || '0');
  const BuildStamp = ref<string>(process.env.BUILD_STAMP || '0');
  const BuildStage = ref<string>('PROD');
  return { packageVersion, BuildStamp, BuildStage };
});
