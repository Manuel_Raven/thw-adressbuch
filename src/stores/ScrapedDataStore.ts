import {
  GetUpdatedPayload,
  GetGeoJson,
  GetLatestCommitInfoFromStorage,
} from './../../shared/UpdateScrapedData';
import {
  ScrapedDienststellen,
  ScrapedDienststellenGeo,
  Commit,
} from './StoreTypes.d';
import { defineStore } from 'pinia';
import { ref } from 'vue';
import distance from '@turf/distance';
// Extend SWR with Periodic Background Sync Typings
interface SWWithPGB extends ServiceWorkerRegistration {
  periodicSync: {
    getTags: () => Promise<string[]>;
    register: (
      string: string,
      option: { minInterval: number }
    ) => Promise<string[]>;
  };
}

export const useScrapedDataStore = defineStore('ScrapedData', () => {
  // Define States
  const data = ref<ScrapedDienststellen[]>([]);
  const storeLoaded = ref(false);
  const LatestCommitInfo = ref<null | Commit>(null);
  const GermanyGeoJson = ref<Record<string, unknown> | null>(null);
  const unitindex = ref<string[]>([]);
  const Teileinheitindex = ref<string[]>([]);
  const TeileinheitCounts = ref<{ [key: string]: number }>({});

  // Define Getters
  const GetByType = (type: string) => {
    return data.value.filter((x) => x.type == type);
  };
  const GetByParent = (parent_name: string) => {
    return data.value.filter((x) => x.parents.includes(parent_name));
  };

  const GetByParentAndType = (parent_name: string, type: string) => {
    return data.value.filter(
      (x) => x.parents.includes(parent_name) && x.type == type
    );
  };

  const FindByName = (name: string) => {
    return data.value.find((x) => x.name == name);
  };

  const SearchByNameOrCode = (search: string, code: string) => {
    return data.value.filter(
      (x) =>
        x.name.toLowerCase().includes(search.toLowerCase()) ||
        x.code.toLowerCase().includes(code.toLowerCase())
    );
  };

  const FindDstByAnyAttribute = (
    name: string | null = null,
    code: string | null = null,
    oeId: string | null = null,
    type: string | null = null,
    parent: string | null = null,
    unit: string | null = null,
    teileinheit: string | null = null,
    geolocation: number[] | null = null
  ) => {
    // Set To null if string is empty
    name = name ? name : null;
    code = code ? code : null;
    type = type ? type : null;
    parent = parent ? parent : null;
    unit = unit ? unit : null;
    teileinheit = teileinheit ? teileinheit : null;

    let result = data.value;

    if (type) {
      result = result.filter((x) => x.type === type);
    }
    if (name || code || oeId) {
      console.log(oeId);
      result = result.filter(
        (x) =>
          x.oeId
            .toString()
            .slice(3)
            .includes(oeId as string) ||
          x.name.toLowerCase().includes((name as string).toLowerCase()) ||
          x.code.toLowerCase().includes((code as string).toLowerCase())
      );
    }

    if (type) {
      result = result.filter((x) => x.type == type);
    }
    if (parent) {
      result = result.filter((x) => x.parents.includes(parent as string));
    }
    if (unit) {
      result = result.filter((x) =>
        x.units.some((y) =>
          y.name.toLowerCase().includes((unit as string).toLowerCase())
        )
      );
    }
    if (teileinheit) {
      result = result.filter((x) =>
        x.units.some((y) =>
          y.teileinheit.some((z) =>
            z.name.toLowerCase().includes((teileinheit as string).toLowerCase())
          )
        )
      );
    }
    if (geolocation) {
      // Add to result distance
      result = result.map((x) => {
        const preffered_coordinates =
          x.osm_data.coordinates || x.coordinates || null;
        if (!preffered_coordinates) return { ...x, distance: -1 };
        const distance_to_geolocation = distance(
          {
            type: 'Point',
            coordinates: [preffered_coordinates[0], preffered_coordinates[1]],
          },
          {
            type: 'Point',
            coordinates: [geolocation[0], geolocation[1]],
          },
          { units: 'kilometers' }
        ).toFixed(2);
        return { ...x, distance_to_geolocation };
      });
      // Sort by distance
      result = (result as ScrapedDienststellenGeo[]).sort(
        (a, b) =>
          (a.distance_to_geolocation || 0) - (b.distance_to_geolocation || 0)
      );
    }

    return result;
  };

  const SortDataByName = () => {
    return data.value.sort((a, b) => {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    });
  };

  const GetByoeID = (oeID: number) => {
    return data.value.find((x) => x.oeId == oeID);
  };

  const SortDataByCode = () => {
    return data.value.sort((a, b) => {
      if (a.code < b.code) return -1;
      if (a.code > b.code) return 1;
      return 0;
    });
  };

  const SortByDataOrCode = (sortBy: string) => {
    if (sortBy == 'name') {
      return SortDataByName();
    } else if (sortBy == 'code') {
      return SortDataByCode();
    } else {
      return SortDataByName();
    }
  };

  const GetUniqueUnitsNamesInTypeOV = () => {
    return data.value
      .filter((x) => x.type == 'OV')
      .map((x) => x.units)
      .flat()
      .map((x) => x.name)
      .filter((x, i, a) => a.indexOf(x) === i);
  };

  const GetEachTeilEinheitenNamesInTypeOV = () => {
    return data.value

      .filter((x) => x.type == 'OV')
      .map((x) => x.units)
      .flat()
      .map((x) => x.teileinheit)
      .flat()
      .map((x) => x.name);
  };

  // const GetUniqueTeilEinheitenNamesInTypeOV = (allnames: string[]) => {
  //   allnames.filter((x, i, a) => a.indexOf(x) === i);
  // };

  const InitIndexes = () => {
    unitindex.value = GetUniqueUnitsNamesInTypeOV().sort();
    TeileinheitCounts.value = GetEachTeilEinheitenNamesInTypeOV().reduce(
      (acc, cur) => {
        acc[cur] = (acc[cur] || 0) + 1;
        return acc;
      },
      {} as { [key: string]: number }
    );
    Teileinheitindex.value = Object.keys(TeileinheitCounts.value).sort();
  };

  const GetGermanyGeoJSON = () => {
    if (GermanyGeoJson.value) {
      // Deep Copy
      return JSON.parse(JSON.stringify(GermanyGeoJson.value));
    }
  };

  return {
    data,
    GetByType,
    GetByParent,
    GetByParentAndType,
    SearchByNameOrCode,
    SortDataByName,
    SortDataByCode,
    SortByDataOrCode,
    GetUniqueUnitsNamesInTypeOV,
    // GetUniqueTeilEinheitenNamesInTypeOV,
    FindDstByAnyAttribute,
    InitIndexes,
    unitindex,
    Teileinheitindex,
    GetByoeID,
    storeLoaded,
    FindByName,
    GermanyGeoJson,
    GetGermanyGeoJSON,
    TeileinheitCounts,
    LatestCommitInfo,
  };
});

const updateContentOnPageLoad = async () => {
  console.log('Update On Page Load');
  const updated = await GetUpdatedPayload();
  const localStore = useScrapedDataStore();
  localStore.data = updated || [];
  localStore.InitIndexes();
  localStore.GermanyGeoJson = await GetGeoJson();
  console.log('Update On Page Load Done');
  localStore.LatestCommitInfo = await GetLatestCommitInfoFromStorage();
  localStore.storeLoaded = true;
};
// Run by App.vue as Initializer
export const InitCustomStore = async () => {
  console.log('Run Init');
  await updateContentOnPageLoad();
  const registration = await navigator.serviceWorker.ready;
  // IF periodic Sync Supported?
  if ('periodicSync' in registration) {
    const tags = await (registration as SWWithPGB).periodicSync.getTags();
    if (!tags.includes('content-sync')) {
      // Try to Setup Periodic Background Sync
      try {
        console.log('Try Register');
        await (registration as SWWithPGB).periodicSync.register(
          'content-sync',
          {
            // An interval of 3 days.
            minInterval: 24 * 60 * 60 * 1000,
          }
        );
      } catch (error) {
        // Periodic background sync cannot be used.
        console.log(error);
      }
    }
  }
};
