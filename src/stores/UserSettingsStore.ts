import { defineStore } from 'pinia';

export const useUserSettingsStore = defineStore('UserSettings', {
  state: () => ({
    pointer_radius: 0.05,
    show_self_on_map: true,
    self_radius: 0.05,
    firstVisit: false,
  }),
  getters: {},
  actions: {},
  persist: true,
});
