import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', redirect: '/search' },
      {
        path: 'statistics',
        component: () => import('pages/IndexPage.vue'),
      },
      {
        path: 'settings',
        component: () => import('src/pages/settingsPage.vue'),
      },
      { path: 'ov', component: () => import('pages/ov.vue') },
      { path: 'rst', component: () => import('pages/rst.vue') },
      { path: 'lv', component: () => import('pages/lv.vue') },
      { path: 'az', component: () => import('pages/az.vue') },
      { path: 'licenses', component: () => import('pages/licensesPage.vue') },
      { path: 'imprint', component: () => import('pages/ImprintPage.vue') },
      { path: 'search', component: () => import('pages/SearchPage.vue') },
      {
        path: '/dst/:id',
        name: 'dst',
        redirect: (to) => {
          return {
            name: 'dst_full',
            params: { id: to.params.id, tab: 'contact' },
          };
        },
      },
      {
        path: '/dst/:id/:tab',
        name: 'dst_full',
        component: () => import('pages/dst.vue'),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
